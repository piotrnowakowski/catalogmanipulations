import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.google.protobuf.InvalidProtocolBufferException;
import com.here.hrn.HRN;
import com.here.nowakows.test.schema.v1.TwoIntegersAndOneStringSchema;
import com.here.platform.data.client.engine.javadsl.DataEngine;
import com.here.platform.data.client.engine.javadsl.ReadEngine;
import com.here.platform.data.client.engine.javadsl.WriteEngine;
import com.here.platform.data.client.javadsl.AdminApi;
import com.here.platform.data.client.javadsl.DataClient;
import com.here.platform.data.client.javadsl.IndexPartition;
import com.here.platform.data.client.javadsl.IndexParts;
import com.here.platform.data.client.javadsl.NewPartition;
import com.here.platform.data.client.javadsl.Partition;
import com.here.platform.data.client.javadsl.QueryApi;
import com.here.platform.data.client.model.AdditionalFields;
import com.here.platform.data.client.model.AutomaticVersionDeletion;
import com.here.platform.data.client.model.ByteRange;
import com.here.platform.data.client.model.CrcAlgorithm;
import com.here.platform.data.client.model.DigestAlgorithm;
import com.here.platform.data.client.model.IndexDefinition;
import com.here.platform.data.client.model.IndexLayerType;
import com.here.platform.data.client.model.IndexType;
import com.here.platform.data.client.model.InteractiveMapLayerType;
import com.here.platform.data.client.model.InteractiveMapProperties;
import com.here.platform.data.client.model.LayerTypes;
import com.here.platform.data.client.model.Partitioning;
import com.here.platform.data.client.model.PendingPartition;
import com.here.platform.data.client.model.Ttl;
import com.here.platform.data.client.model.VersionDependency;
import com.here.platform.data.client.model.Volumes;
import com.here.platform.data.client.model.WritableCatalogConfiguration;
import com.here.platform.data.client.model.WritableLayer;
import com.here.platform.data.client.settings.ConsumerSettings;

import akka.Done;
import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.actor.CoordinatedShutdown;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.util.ByteString;

public class OverviewJavaMain {

    ActorSystem actorSystem = ActorSystem.create();
    private final HRN hrn = HRN.fromString("hrn:here:data::olp-here:group-cat-task-1-1");

    public static void main(String[] args) {
        OverviewJavaMain main = new OverviewJavaMain();

        //        main.readCatalogs();

        //        main.readObjectLayer("test-key-1",
        //                                     "layer-3-task-1-1");

        //        main.createCatalog("test-1");

        //        main.publishToVersionedLayer(HRN.fromString("hrn:here:data::olp-here:creating-catalog-test-test-1"),
        //                                     "layer-0",
        //                                     "partition-2",
        //                                     createVersionedPartitionData());

        //        main.publishToIndexedLayer(HRN.fromString("hrn:here:data::olp-here:creating-catalog-test-test-1"),
        //                                     "layer-2",
        //                                     "partition-0-in-indexed-layer",
        //                                     createIndexedPartitionData());

        //        main.queryIndexLayer(HRN.fromString("hrn:here:data::olp-here:creating-catalog-test-test-1"),
        //        "layer-2");

        //        main.queryVersionedLayer(HRN.fromString("hrn:here:data::olp-here:creating-catalog-test-test-1"),
        //                                 "layer-0",
        //                                 11,
        //                                 "partition-0", "partition-1", "partition-2");

        //        main.publishDataToVersionedLayerWithSchema(HRN.fromString("hrn:here:data::olp-here:catalog-task-1-3"),
        //                                                   "versioned-layer-with-schema", "partition-0",
        //                                                   createDataWithSchemaBuilder());

        //        main.publishDataToVersionedLayerWithSchema(HRN.fromString("hrn:here:data::olp-here:catalog-task-1-3"),
        //                                                   "versioned-layer-with-schema", "partition-1",
        //                                                   createDataNotMatchingSchema());

        //        main.queryVersionedLayer(HRN.fromString("hrn:here:data::olp-here:catalog-task-1-3"),
        //                                 "versioned-layer-with-schema",
        //                                 1,
        //                                 true,
        //                                 "partition-0", "partition-1");

        main.publishMultiplePartitionsToStreamLayer(100,
                                                    HRN.fromString("hrn:here:data::olp-here:task-2-catalog-with-stream-layer"),
                                                    "task-2-subtask-1-stream-leayer");
    }

    private void publishMultiplePartitionsToStreamLayer(int numberOfPartitions, HRN catalogHrn, String layerId) {
        List<String> partitionIdsList = new ArrayList<>();
        List<byte[]> partitionData = new ArrayList<>();
        for (int i = 0; i < numberOfPartitions ; i++) {
            partitionIdsList.add(String.valueOf(i));
            partitionData.add(createStreamPartitionData(i));
        }
        publishPartitionsToStreamLayer(catalogHrn, layerId, partitionIdsList, partitionData);
    }

    private void publishPartitionsToStreamLayer(HRN catalogHrn, String layer, List<String> newPartitionsIdList,
                                                List<byte[]> blobDataList) {
        // create writeEngine and queryApi for a catalog
        QueryApi queryApi = DataClient.get(actorSystem).queryApi(catalogHrn);
        WriteEngine writeEngine = DataEngine.get(actorSystem).writeEngine(catalogHrn);

        // subscribe to receive new publications from stream layer
        queryApi.subscribe(
            layer,
            new ConsumerSettings.Builder().withGroupName("test-consumer").build(),
            partition -> processPartition(partition));

        ArrayList<PendingPartition> partitionList = new ArrayList<>();
        int i = 0;
        for (String partitionId: newPartitionsIdList) {
            NewPartition newPartition =
                new NewPartition.Builder()
                    .withPartition(partitionId)
                    .withData(blobDataList.get(i++))
                    .withLayer(layer)
                    .build();
            partitionList.add(newPartition);
        }

        Source<PendingPartition, NotUsed> partitions = Source.from(partitionList);

        writeEngine.publish(partitions);

    }

    private void processPartition(Partition partition) {
        System.out.println("got data from stream layer: " + partition);
    }

    private byte[] createStreamPartitionData(int i) {
        return TwoIntegersAndOneStringSchema.MainProtobufMessage.newBuilder()
                                                                .setFirstInt(i)
                                                                .setSecondInt(i * 100)
                                                                .setTheString("Hello Again Schema World: " + i).build()
                                                                .toByteArray();
    }

    private static byte[] createDataNotMatchingSchema() {
        return "Hello world!".getBytes();
    }

    private void publishDataToVersionedLayerWithSchema(HRN catalogHrn, String layerId,
                                                       String partitionKey, byte[] blobData) {
        publishPartitionsToVersionedLayer(catalogHrn, layerId, partitionKey, blobData);

    }

    private static byte[] createDataWithSchemaBuilder() {
        return TwoIntegersAndOneStringSchema.MainProtobufMessage.newBuilder()
                                                                .setFirstInt(1)
                                                                .setSecondInt(1)
                                                                .setTheString("Hello Schema World").build()
                                                                .toByteArray();
    }

    private void queryVersionedLayer(HRN catalogHrn, String layerId, long version, String... partitions) {
        queryVersionedLayer(catalogHrn, layerId, version, false, partitions);
    }

    private void queryVersionedLayer(HRN catalogHrn, String layerId, long version, boolean
                                                                                       decodeSchema,
                                     String... partitions) {
        List<String> partitionIdsList = Arrays.asList(partitions);

        QueryApi queryApi = DataClient.get(actorSystem).queryApi(catalogHrn);

        // parallelism defines how many parallel requests would be made to fetch the data
        int parallelism = 10;

        // create readEngine for source catalog
        ReadEngine readEngine = DataEngine.get(actorSystem).readEngine(catalogHrn);

        //         stream of tuple of (partition, bytes)
        CompletionStage<Source<Pair<Partition, byte[]>, NotUsed>> dataAsBytes =
            queryApi
                .getPartitions(version, layerId, AdditionalFields.AllFields())
                .thenApply(
                    metadata ->
                        metadata.filter(partition -> partitionIdsList.contains(partition.getPartition()))
                                .mapAsync(
                                    parallelism,
                                    partition ->
                                        readEngine
                                            .getDataAsBytes(partition)
                                            .thenApply(data -> new Pair<>(partition, data))));

        dataAsBytes.whenComplete((source, notUsed) -> {
            source.runWith(Sink.foreach(
                param -> System.out.println(param.first().getPartition() + ": " + getPartitionData(decodeSchema,
                                                                                                   param.second()))),
                           Materializer.matFromSystem(actorSystem));
        });

        //        queryApi
        //            .getPartitionsById(version, layerId, partitionIdsList, AdditionalFields.AllFields())
        //            .whenComplete((partitionList, throwable) -> partitionList.forEach(System.out::println));
    }

    private String getPartitionData(boolean decodeSchema, byte[] data) {
        if (decodeSchema) {
            try {
                return TwoIntegersAndOneStringSchema.MainProtobufMessage.newBuilder()
                                                                        .mergeFrom(data)
                                                                        .build()
                                                                        .toString();
            } catch (InvalidProtocolBufferException e) {
                //                e.printStackTrace();
                return "Exception on schema decoding: " + new String(data);
            }
        } else {
            return new String(data);
        }
    }

    private void publishToIndexedLayer(HRN catalogHrn, String indexLayerId,
                                       String partitionId, byte[] indexedPartitionData) {

        WriteEngine writeEngine = DataEngine.get(actorSystem).writeEngine(catalogHrn);

        // parallelism defines how many parallel requests would be made to fetch the data
        int parallelism = 10;

        // list of dependencies for this publication
        List<VersionDependency> dependencies = Collections.emptyList();

        NewPartition newPartition =
            new NewPartition.Builder()
                .withPartition("")
                .withLayer(indexLayerId)
                .withData(indexedPartitionData)
                .addIntField("someIntKey", 42)
                .addStringField("someStringKey", "abc")
                .addBooleanField("someBooleanKey", true)
                .addTimeWindowField("someTimeWindowKey", 123456789L)
                .addHereTileField("someHereTileKey", 91956L)
                .addMetadata("someKey1", "someValue1")
                .addMetadata("someKey2", "someValue2")
                .withChecksum(Optional.empty())
                .withDataSize(OptionalLong.of(0))
                .build();

        Iterator<NewPartition> partitions = Arrays.asList(newPartition).iterator();
        CompletionStage<Done> publish = writeEngine.uploadAndIndex(partitions).whenComplete((done, throwable) -> {
            System.out.println("Publish and indexing complete!");
        });

    }

    private void queryIndexLayer(HRN catalogHrn, String indexLayerId) {
        QueryApi queryApi = DataClient.get(actorSystem).queryApi(catalogHrn);
        ReadEngine readEngine = DataEngine.get(actorSystem).readEngine(catalogHrn);
        int numberOfParts = 50;

        IndexParts indexParts =
            queryApi.queryIndexParts(indexLayerId, numberOfParts).toCompletableFuture().join();

        // How to query the index layer
        String queryString = "someIntKey==42";

        Source<IndexPartition, NotUsed> indexPartitionsSource =
            Source.from(indexParts.getParts())
                  .mapAsync(10, part -> queryApi.queryIndex(indexLayerId, queryString, part))
                  .flatMapConcat(s -> s);

        ActorMaterializer actorMaterializer = ActorMaterializer.create(actorSystem);

        System.out.println(
            "Download the data corresponding to the index partitions previously found by the queryIndex method");

        int parallelism = 10;
        indexPartitionsSource
            .mapAsyncUnordered(parallelism, readEngine::getDataAsBytes)
            // Replace the method youCanProcessTheDataHere with your own code.
            .runForeach(this::youCanProcessTheDataHere, actorMaterializer)
            .toCompletableFuture()
            .join();

        System.out.println(
            "Computation finished. Shutting down the HTTP connections and the actor system.");
        CoordinatedShutdown.get(actorSystem)
                           .runAll(CoordinatedShutdown.unknownReason())
                           .toCompletableFuture()
                           .join();
    }

    private void youCanProcessTheDataHere(Object o) {
        System.out.println(o);
    }

    private static byte[] createIndexedPartitionData() {
        return ("{\"someIntKey\": 42"
                + "}").getBytes();
    }

    private static byte[] createVersionedPartitionData() {
        return "some new content for partition 2".getBytes();
    }

    private void publishPartitionsToVersionedLayer(HRN catalogHrn, String layer, String newPartitionId,
                                                   byte[] blobData) {
        publishPartitionsToVersionedLayer(catalogHrn, layer, Collections.singletonList(newPartitionId),
                                          Collections.singletonList(blobData));

    }

    private void publishPartitionsToVersionedLayer(HRN catalogHrn, String layer, List<String> newPartitionsIdList,
                                                   List<byte[]> blobDataList) {
        // create writeEngine for source catalog
        WriteEngine writeEngine = DataEngine.get(actorSystem).writeEngine(catalogHrn);

        // parallelism defines how many parallel requests would be made to fetch the data
        int parallelism = 10;

        // list of dependencies for this publication
        List<VersionDependency> dependencies = Collections.emptyList();

        int i = 0;
        ArrayList<PendingPartition> partitionList = new ArrayList<>();
        for (String partitionId: newPartitionsIdList) {
            NewPartition newPartition =
                new NewPartition.Builder()
                    .withPartition(partitionId)
                    .withData(blobDataList.get(i++))
                    .withLayer(layer)
                    .build();
            partitionList.add(newPartition);
        }


        Source<PendingPartition, NotUsed> partitions = Source.from(partitionList);

        CompletableFuture<Done> futurePublish =
            writeEngine
                .publishBatch2(parallelism, Optional.of(Arrays.asList(layer)), dependencies, partitions)
                .toCompletableFuture();
    }

    private void createCatalog(String someIdentifierSuffix) {
        String catalogId = "creating-catalog-test-" + someIdentifierSuffix;
        WritableCatalogConfiguration catalogConfig =
            new WritableCatalogConfiguration.Builder()
                .withId(catalogId)
                .withName(catalogId)
                .withSummary("This is a catalog summary.")
                .withDescription("This is what the catalog is for.")
                .withTags(new HashSet<>(Arrays.asList("tag1", "tag2", "tag3")))
                .withAutomaticVersionDeletion(
                    AutomaticVersionDeletion.builder().withNumberOfVersionsToKeep(10L).build())
                .withLayers(
                    Arrays.asList(
                        new WritableLayer.Builder()
                            .withId("layer-0")
                            .withName("INFO")
                            .withSummary("This is a layer summary.")
                            .withDescription("This is a layer description.")
                            .withLayerType(LayerTypes.Versioned())
                            .withPartitioning(Partitioning.Generic())
                            .withVolume(Volumes.Durable())
                            .withContentType("application/x-protobuf")
                            .withDigest(DigestAlgorithm.SHA256())
                            .withCrc(CrcAlgorithm.CRC32C()),
                        new WritableLayer.Builder()
                            .withId("layer-1")
                            .withName("SCHEMAS")
                            .withSummary("This is a layer summary.")
                            .withDescription("This is a layer description.")
                            .withLayerType(LayerTypes.Versioned())
                            .withPartitioning(Partitioning.Generic())
                            .withVolume(Volumes.Durable())
                            .withContentType("application/x-protobuf"),
                        new WritableLayer.Builder()
                            .withId("layer-2")
                            .withName("INDEX")
                            .withSummary("This is a layer summary.")
                            .withDescription("This is a layer description.")
                            .withLayerType(
                                new IndexLayerType.Builder()
                                    .addIndexDefinition(
                                        new IndexDefinition.Builder()
                                            .withName("someIntKey")
                                            .withIndexType(IndexType.Int)
                                            .build())
                                    .addIndexDefinition(
                                        new IndexDefinition.Builder()
                                            .withName("someStringKey")
                                            .withIndexType(IndexType.String)
                                            .build())
                                    .addIndexDefinition(
                                        new IndexDefinition.Builder()
                                            .withName("someTimeWindowKey")
                                            .withIndexType(IndexType.TimeWindow)
                                            .withDuration(3600000L)
                                            .build())
                                    .addIndexDefinition(
                                        new IndexDefinition.Builder()
                                            .withName("someHereTileKey")
                                            .withIndexType(IndexType.HereTile)
                                            .withZoomLevel(8)
                                            .build())
                                    .withTtl(Ttl.OneMonth)
                                    .build())
                            .withPartitioning(Partitioning.NoPartition())
                            .withVolume(Volumes.Durable())
                            .withContentType("application/x-protobuf"),
                        new WritableLayer.Builder()
                            .withId("layer-3")
                            .withName("OBJECTSTORE")
                            .withSummary("This is layer summary")
                            .withDescription("This is layer description")
                            .withLayerType(LayerTypes.ObjectStore())
                            .withPartitioning(Partitioning.NoPartition())
                            .withVolume(Volumes.Durable()),
                        new WritableLayer.Builder()
                            .withId("layer-4")
                            .withName("INTERACTIVEMAP")
                            .withSummary("This is layer summary")
                            .withDescription("This is layer description")
                            .withLayerType(
                                new InteractiveMapLayerType.Builder()
                                    .withInteractiveMapProperties(
                                        new InteractiveMapProperties.Builder()
                                            .withSearchableProperties(
                                                Arrays.asList("some-property1", "some-property-2"))
                                            .build())
                                    .build())
                            .withPartitioning(Partitioning.NoPartition())
                            .withVolume(Volumes.Durable())
                            .withContentType("application/geo+json")))
                .build();

        AdminApi adminApi = DataClient.get(actorSystem).adminApi();
        adminApi
            .createCatalog(catalogConfig)
            .thenApply(
                hrn -> {
                    System.out.println("Created new catalog `" + hrn + "`");
                    return processCreatedCatalog(hrn);
                });
    }

    private Object processCreatedCatalog(HRN hrn) {
        return null;
    }

    private void readCatalogs() {
        DataClient dataClient = DataClient.get(actorSystem);
        AdminApi adminApi = dataClient.adminApi();

        // Now you can start using the API
        adminApi
            .listCatalogs()
            .whenComplete(
                (catalogHRNs, e) -> {
                    if (catalogHRNs != null) {
                        catalogHRNs.forEach(System.out::println);
                    } else if (e != null) {
                        e.printStackTrace();
                    }
                })
            // When done, shutdown the Data Client through the ActorSystem
            .thenAccept(
                unbound ->
                    CoordinatedShutdown.get(actorSystem).run(CoordinatedShutdown.unknownReason()));
    }

    private void readObjectLayer(String key, String layer) {
        // create readEngine
        ReadEngine readEngine = DataEngine.get(actorSystem).readEngine(hrn);

        // full object as dataSource
        CompletionStage<Source<ByteString, NotUsed>> dataAsSource =
            readEngine.getObjectDataAsSource(layer, key, ByteRange.all());

        dataAsSource.whenComplete((object, e) -> {
            if (object != null) {
                object
                    .runForeach(data -> System.out.println(data.utf8String()), Materializer.matFromSystem(actorSystem));
            }
        }).thenAccept(
            unbound ->
                CoordinatedShutdown.get(actorSystem).run(CoordinatedShutdown.unknownReason()));

        //        // partial object with provided range as dataSource
        //        CompletionStage<Source<ByteString, NotUsed>> dataAsSourceWithRange =
        //            readEngine.getObjectDataAsSource(layer, key, ByteRange.fromRange(5, 10));
        //
        //        // full object as an Array of bytes
        //        CompletionStage<byte[]> dataAsByteArray =
        //            readEngine.getObjectDataAsBytes(layer, key, ByteRange.all());
        //
        //        // partial object as an Array of bytes with provided range
        //        CompletionStage<byte[]> dataAsByteArrayWithRange =
        //            readEngine.getObjectDataAsBytes(layer, key, ByteRange.fromRange(5, 10));

    }

    private void writeObjectToCatalog(String key, String layer, byte[] blobData) {
        WriteEngine writeEngine = DataEngine.get(actorSystem).writeEngine(hrn);

        CompletableFuture<Void> futureUploadObject =
            writeEngine
                .uploadObject(
                    layer,
                    key,
                    new com.here.platform.data.client.scaladsl.NewPartition.ByteArrayData(blobData),
                    Optional.empty(),
                    Optional.empty())
                .toCompletableFuture()
                .thenAccept(
                    unbound ->
                        CoordinatedShutdown.get(actorSystem).run(CoordinatedShutdown.unknownReason()));
    }

}
